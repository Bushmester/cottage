class Cottage():
    def __init__(self, price, people_count, sleep_place_number, district, distance_from_the_city, additional_services):
        self.price = price
        self.people_count = people_count
        self.sleep_place_number = sleep_place_number
        self.district = district
        self.distance_from_the_city = distance_from_the_city
        self.additional_services = additional_services

    def get_price(self, price):
        return self.price

    def get_people_count(self, people_count):
        return self.people_count

    def get_sleep_place_number(self, sleep_place_number):
        return self.sleep_place_number


class AdditionalServices(Cottage):
    def __init__(self, pool, sauna, billiards, beach, fireplace, gazebo, ping_pong):
        self.pool = pool
        self.sauna = sauna
        self.billiards = billiards
        self.beach = beach
        self.fireplace = fireplace
        self.gazebo = gazebo
        self.ping_pong = ping_pong

    def get_pool(self, pool):
        return self.pool

    def get_sauna(self, sauna):
        return self.sauna

    def get_billiards(self, billiards):
        return self.billiards

    def get_beach(self, beach):
        return self.beach

    def get_fireplace(self, fireplace):
        return self.fireplace

    def get_billiards(self, gazebo):
        return self.gazebo

    def get_ping_pong(self, ping_pong):
        return self.ping_pong


class Customer():
    def __init__(self, full_name, budget, people_count, sleep_place_number, district):
        self.full_name = full_name
        self.price = price
        self.people_count = people_count
        self.sleep_place_number = sleep_place_number
        self.district = district

    def get_full_name(self, full_name):
        return self.full_name

    def get_budget(self, budget):
        return self.budget

    def get_people_count(self, people_count):
        return self.people_count

    def get_district(self, district):
        return self.district

    def get_sleep_place_number(self, sleep_place_number):
        return self.sleep_place_number


class Food(Customer):
    def __init__(self, snacks, pizza, burger, sushi):
        self.snacks = snacks * SNACK_PRICE
        self.pizza = pizza * PIZZA_PRICE
        self.burger = burger * BURGR_PRICE
        self.sushi = sushi * SUSHI_PRICE

    def get_snacks(self, snacks):
        return self.snacks

    def get_pizza(self, pizza):
        return self.pizza

    def get_food_price(self, snacks, pizza, burger, sushi):
        price = snacks + pizza + burger + sushi
        return price


class Drinks(Customer):
    def __init__(self, alcoholic, not_alcoholic):
        self.alcoholic = alcoholic * ALCOHOLIC_PRICE
        self.not_alcoholic = not_alcoholic * NOT_ALCOHOLIC_PRICE

    def get_alcoholic(self, alcoholic):
        return self.alcoholic

    def get_not_alcoholic(self, not_alcoholic):
        return self.not_alcoholic

    def get_drink_price(self, alcoholic, not_alcoholic):
        price = alcoholic + not_alcoholic
        return price
